package blogapp.services;

import blogapp.exception.BlogExceptions;
import blogapp.model.User;
import blogapp.repository.UserRepository;

import blogapp.services.UserService;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;


import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.lang.reflect.Array;
import java.util.Arrays;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    private UserRepository userRepository;
    private BCryptPasswordEncoder mockbcryptpassword;
    private UserService userServiceUnderTest;
    private User user;
    private User user1;
    private User user2;

    @Before
    public void setUp(){
        userRepository = mock(UserRepository.class);
        mockbcryptpassword = mock(BCryptPasswordEncoder.class);
        userServiceUnderTest = new UserService(userRepository,mockbcryptpassword);

        user = new User("Johgn","johgn@out.com","123");
        user1 = new User("Janny", "jany@gmail.com","678");
        user2 = new User("Ryan", "ryan@out.com", "957");

        when(userServiceUnderTest.save(user)).thenReturn(user);
        when(userServiceUnderTest.getAllUser()).thenReturn(Arrays.asList(user,user1,user2));

    }
@Test
public void test_save(){ final String email = "johgn@out.com";
    User user_check = userServiceUnderTest.save(user);
    assertEquals(user_check.getEmail(),email);
    verify(userRepository).save(user);
}
@Test
public void test_getAllUser(){
        userServiceUnderTest.getAllUser();

}


}
