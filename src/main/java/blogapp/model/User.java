package blogapp.model;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Entity
@Table(name = "user")
@DynamicUpdate
public class User {
    @Id
    @GeneratedValue
    private Long user_id;
    @NotBlank(message = "Mandatory")
    private String user_name;
    @NotBlank(message = "Mandatory")
    private String email;
    @NotBlank(message = "Mandatory")
    private String password;

    private String image_file;

    @OneToMany(cascade = CascadeType.ALL, targetEntity = Posts.class)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private Set<Posts> posts;

    public Long getUser_id() {
        return user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswsord() {
        return password;
    }

    public void setPasswsord(String passwsord) {
        this.password = passwsord;
    }

    public String getImage_file() {
        return image_file;
    }

    public void setImage_file(String image_file) {
        this.image_file = image_file;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public User(String user_name, String email, String passwsord) {
        this.user_name = user_name;
        this.email = email;
        this.password = passwsord;
    }

    public User() {
    }
}
