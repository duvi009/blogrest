package blogapp.controller;

import blogapp.exception.BlogExceptions;
import blogapp.model.Greeting;
import blogapp.model.User;
import blogapp.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.impl.StaticLoggerBinder;
import sun.rmi.runtime.Log;

import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("/users")
public class UserController {
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired
    UserService service;

    @PersistenceContext
    EntityManager entityManager;

    /*
    @PostMapping("/addnew")
    public ResponseEntity<User> createUpdateUser(User user) throws BlogExceptions {
        User updated = service.createUpdateUser(user);
        return new ResponseEntity<User>(updated, new HttpHeaders(), HttpStatus.OK);
    }*/

    @RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        return new Greeting(counter.incrementAndGet(),
                String.format(template, name));
    }

    @PostMapping("/save")
    public ResponseEntity<User> save(@Valid @RequestBody User user) throws BlogExceptions {
        User saved = service.save(user);
        return new ResponseEntity<User>(saved, new HttpHeaders(), HttpStatus.OK);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<User> update(@PathVariable Long id, @Valid @RequestBody User user) throws BlogExceptions {
        if (!service.findUserById(id).isPresent()) {
            log.error("Id : " + id + " is not available");
            ResponseEntity.badRequest().build();
        }
        User user_retreve = service.findUserById(id).get();
        user.setUser_id(id);
        return ResponseEntity.ok(service.save(user));
    }

    @GetMapping("/userall")
    public ResponseEntity<List<User>> getAllUser() {
        List<User> userList = service.getAllUser();
        return new ResponseEntity<List<User>>(userList, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/getuseremail/{email}")
    public ResponseEntity<User>getbyemail(@PathVariable String email) throws BlogExceptions{
        return ResponseEntity.ok(service.get_user_by_email(email));
    }

    @GetMapping("/userbyid/{id}")
    public ResponseEntity<User> getuserById(@PathVariable Long id) throws BlogExceptions {
        Optional<User> users = service.findUserById(id);
        if (!users.isPresent()) {
            log.error("Id : " + id + "is not available");
            ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(users.get());
    }
}
